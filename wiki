^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                   SIMULATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

TERMINALE 1:
------------

0) System upload:

ssh ubuntu@192.168.80.20
password: ubuntu

roscore

TERMINALE 2:
------------

0) Connection:

rosrun challenge_arena EMARC_connect.sh

echo $ROS_MASTER_URI --> http://192.168.80.20:11311/


1) Gazebo:

roslaunch objects_sorting_test gazebo_setup.launch 

2) MoveIt:

roslaunch ur10_objects_sorting_moveit_config ur10_objects_sorting_moveit_planning_execution.launch sim:=true

3) Rviz:

roslaunch ur10_objects_sorting_moveit_config moveit_rviz.launch config:=true

4) Apriltag:

roslaunch objects_sorting_test apriltag.launch

5) Objects sorting test:

roslaunch objects_sorting_test sorting_test_sim.launch

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                   REAL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

TERMINALE 1:
-----------

0) System upload:

ssh ubuntu@192.168.80.20
password: ubuntu

1) Kinect: 

roslaunch kinect2_bridge kinect2_bridge_ir.launch sensor_name:=camera

TERMINALE 2:
-----------

0) Real robot:

roslaunch objects_sorting_test real_world_setup.launch

1) MoveIt:

roslaunch ur10_objects_sorting_moveit_config ur10_objects_sorting_moveit_planning_execution.launch sim:=false

2) Rviz:

roslaunch ur10_objects_sorting_moveit_config moveit_rviz.launch config:=true

3) Apriltag:

roslaunch objects_sorting_test apriltag.launch

4) Objects sorting test:

roslaunch objects_sorting_test sorting_test_real.launch
