#include "objects_sorting_test/test.h"

#include <ros/package.h>

#include <fstream>
#include <iomanip>
#include <ctime>
#include <chrono>

#define SIM_DATASET_FOLDER "/dataset_sim/"


using namespace objects_sorting_test;

moveit::planning_interface::MoveGroup* group;

ros::Publisher planning_scene_diff_publisher;
ros::Subscriber sub;

vector<apriltags_ros::AprilTagDetection> april_tags_vector_;
bool april_tag_acquired_ = false;


geometry_msgs::PoseStamped world_cube_pose, camera_cube_pose;

bool pose_established = false;

void getAprilTagPose(const apriltags_ros::AprilTagDetectionArray::ConstPtr &tag_detections)
{

    april_tags_vector_.clear();

    if (tag_detections->detections.size() > 0 && !pose_established){

     ROS_INFO("AprilTAGDETECTIONS= %u!!!!", tag_detections->detections.size());

     for(unsigned int i = 0; i < tag_detections->detections.size(); i++){

       try
       {
         ROS_INFO("Detected cube - Transforming pose!");
         tf::TransformListener listener;
         listener.waitForTransform("world", "camera_ir_optical_frame", ros::Time::now(), ros::Duration(1.0));
         camera_cube_pose = tag_detections->detections.at(i).pose;
         listener.transformPose("world", camera_cube_pose, world_cube_pose);
         ROS_INFO("Detected cube - Pose transformation done!");

         april_tags_vector_.push_back(tag_detections->detections[i]);

         april_tags_vector_.back().pose = world_cube_pose;

         }
         catch (tf::TransformException ex)
         {
           ROS_ERROR("Nope! %s", ex.what());
           pose_established = false;
         }

       }
    }

    april_tag_acquired_ = true;
}

void initParam(ros::NodeHandle node_handle, bool& sim, bool& random,
               double& manipulation_offset, double& pre_manipulation_offset, double& place_offset,
               double& offset_x, double& offset_y, double& offset_z,
               double& red_box_x, double& red_box_y, double& red_box_z,
               double& blue_box_x, double& blue_box_y, double& blue_box_z,
               double& length, double& width, double& height, double& cube_edge,
               string& box_ref_frame){

  if (node_handle.hasParam("sim"))
    node_handle.getParam("sim", sim);
  else{
    ROS_ERROR("Failed to get param 'sim' (boolean). Setting 'sim' to 'true'.");
    sim = true;
  }

  if (node_handle.hasParam("random"))
    node_handle.getParam("random", random);
  else{
    ROS_ERROR("Failed to get param 'random' (boolean). Setting 'random' to 'true'.");
    random = true;
  }

  if (node_handle.hasParam("manipulation_offset"))
    node_handle.getParam("manipulation_offset", manipulation_offset);
  else{
    ROS_ERROR("Failed to get param 'manipulation_offset' (in meters). Setting 'manipulation_offset' to 0.18 m.");
    manipulation_offset = 0.18;
  }

  if (node_handle.hasParam("pre_manipulation_offset"))
    node_handle.getParam("pre_manipulation_offset", pre_manipulation_offset);
  else{
    ROS_ERROR("Failed to get param 'pre_manipulation_offset' (in meters). Setting 'pre_manipulation_offset' to 0.3 m.");
    pre_manipulation_offset = 0.3;
  }

  if (node_handle.hasParam("place_offset"))
    node_handle.getParam("place_offset", place_offset);
  else{
    ROS_ERROR("Failed to get param 'place_offset' (in meters). Setting 'place_offset' to 0.5 m.");
    place_offset = 0.5;
  }

  if (node_handle.hasParam("offset_x"))
    node_handle.getParam("offset_x", offset_x);
  else{
    ROS_ERROR("Failed to get param 'offset_x' (in meters). Setting 'offset_x' to 0.03 m.");
    offset_x = 0.03;
  }

  if (node_handle.hasParam("offset_y"))
    node_handle.getParam("offset_y", offset_y);
  else{
    ROS_ERROR("Failed to get param 'offset_y' (in meters). Setting 'offset_y' to 0.015 m.");
    offset_y = 0.015;
  }

  if (node_handle.hasParam("offset_z"))
    node_handle.getParam("offset_z", offset_z);
  else{
    ROS_ERROR("Failed to get param 'offset_z' (in meters). Setting 'offset_z' to -0.05 m.");
    offset_z = -0.05;
  }

  if (node_handle.hasParam("red_box_x"))
    node_handle.getParam("red_box_x", red_box_x);
  else{
    ROS_ERROR("Failed to get param 'red_box_x' (in meters). Setting 'red_box_x' to -0.060177 m.");
    red_box_x = -0.060177;
  }

  if (node_handle.hasParam("red_box_y"))
    node_handle.getParam("red_box_y", red_box_y);
  else{
    ROS_ERROR("Failed to get param 'red_box_y' (in meters). Setting 'red_box_y' to 0.760087 m.");
    red_box_y = 0.760087;
  }

  if (node_handle.hasParam("red_box_z"))
    node_handle.getParam("red_box_z", red_box_z);
  else{
    ROS_ERROR("Failed to get param 'red_box_z' (in meters). Setting 'red_box_z' to 0.930125 m.");
    red_box_z = 0.930125;
  }

  if (node_handle.hasParam("blue_box_x"))
    node_handle.getParam("blue_box_x", blue_box_x);
  else{
    ROS_ERROR("Failed to get param 'blue_box_x' (in meters). Setting 'blue_box_x' to 0.301077 m.");
    blue_box_x = 0.301077;
  }

  if (node_handle.hasParam("blue_box_y"))
    node_handle.getParam("blue_box_y", blue_box_y);
  else{
    ROS_ERROR("Failed to get param 'blue_box_y' (in meters). Setting 'blue_box_y' to 0.760087 m.");
    blue_box_y = 0.760087;
  }

  if (node_handle.hasParam("blue_box_z"))
    node_handle.getParam("blue_box_z", blue_box_z);
  else{
    ROS_ERROR("Failed to get param 'blue_box_z' (in meters). Setting 'blue_box_z' to 0.930128 m.");
    blue_box_z = 0.930128;
  }

  if (node_handle.hasParam("length"))
    node_handle.getParam("length", length);
  else{
    ROS_ERROR("Failed to get param 'length' (in meters). Setting 'length' to 0.58 m.");
    length = 0.58;
  }

  if (node_handle.hasParam("width"))
    node_handle.getParam("width", width);
  else{
    ROS_ERROR("Failed to get param 'width' (in meters). Setting 'width' to 0.29 m.");
    width = 0.29;
  }

  if (node_handle.hasParam("height"))
    node_handle.getParam("height", height);
  else{
    ROS_ERROR("Failed to get param 'height' (in meters). Setting 'height' to 0.29 m.");
    height = 0.29;
  }

  if (node_handle.hasParam("cube_edge"))
    node_handle.getParam("cube_edge", cube_edge);
  else{
    ROS_ERROR("Failed to get param 'cube_edge' (in meters). Setting 'cube_edge' to 0.1 m.");
    cube_edge = 0.1;
  }

  if (node_handle.hasParam("box_ref_frame"))
    node_handle.getParam("box_ref_frame", box_ref_frame);
  else{
    ROS_ERROR("Failed to get param 'box_ref_frame'. Setting 'box_ref_frame' to '\world'.");
    box_ref_frame = "/world";
  }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "sorting_test");

    ros::NodeHandle node_handle("~");
    ros::NodeHandle n;

    double offset_x, offset_y, offset_z, manipulation_offset, pre_manipulation_offset, place_offset;
    double red_box_x, red_box_y, red_box_z, blue_box_x, blue_box_y, blue_box_z;
    double length, width, height, cube_edge;
    string box_ref_frame;
    bool sim, random;

    initParam(node_handle, sim, random,
              manipulation_offset, pre_manipulation_offset, place_offset,
              offset_x, offset_y, offset_z,
              red_box_x, red_box_y, red_box_z,
              blue_box_x, blue_box_y, blue_box_z,
              length, width, height, cube_edge,
              box_ref_frame);

    string output_file;
    string path = ros::package::getPath("objects_sorting_test");

    //find current time, convert to broken-down time
    chrono::system_clock::time_point now =  chrono::system_clock::now();
    time_t now_current = chrono::system_clock::to_time_t(now);

    //timestamp in the format "%F_%T"
    char timestamp[15];
    std::strftime(timestamp, sizeof(timestamp), "%y%m%d_%H%M%S", std::localtime(&now_current));

    output_file = path + SIM_DATASET_FOLDER + timestamp + ".csv";

    cout << output_file << endl;

    SortingTest sorting_test;

    ros::AsyncSpinner spinner(2);
    spinner.start();

    group = new moveit::planning_interface::MoveGroup("manipulator");


    bool success;

    //Define Boxes Poses
    //^^^^^^^^^^^^^^^^^^^

    geometry_msgs::PoseStamped red_box_pose, blue_box_pose;
    red_box_pose.header.frame_id = box_ref_frame;
    red_box_pose.pose.position.x = red_box_x;
    red_box_pose.pose.position.y = red_box_y;
    red_box_pose.pose.position.z = red_box_z;
    red_box_pose.pose.orientation.w = 1.0;

    blue_box_pose.header.frame_id = box_ref_frame;
    blue_box_pose.pose.position.x = blue_box_x;
    blue_box_pose.pose.position.y = blue_box_y;
    blue_box_pose.pose.position.z = blue_box_z;
    blue_box_pose.pose.orientation.w = 1.0;

    sorting_test.defineRedBox(red_box_pose);
    sorting_test.defineBlueBox(blue_box_pose);

    //Add Collision Objects
    //^^^^^^^^^^^^^^^^^^^^^

    std::vector<moveit_msgs::CollisionObject> collision_objects;

    planning_scene_diff_publisher = n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
    while(planning_scene_diff_publisher.getNumSubscribers() < 1)
    {
      ros::WallDuration sleep_t(0.5);
      sleep_t.sleep();
    }

    ROS_INFO("Adding collision objects into the world.");

    moveit_msgs::PlanningScene planning_scene;
    moveit_msgs::CollisionObject table_0, table_1, box_0, box_1;

    sorting_test.addCollisionBox(table_0, group->getPlanningFrame(), "table0", 0.8, 0.8, 2, 0.124231, -1,  1, 0.0,0.0,0.0,1.0);
    planning_scene.world.collision_objects.push_back(table_0);

    sorting_test.addCollisionBox(table_1, group->getPlanningFrame(), "table1", 0.913, 0.913,  0.78, 0.124231, 0.826114, 0.39, 0.0,0.0,0.0,1.0);
    planning_scene.world.collision_objects.push_back(table_1);


    Eigen::Vector3d scale(0.1, 0.05, 0.05);

    sorting_test.addCollisionMesh(box_0, group->getPlanningFrame(), "box0", "package://objects_sorting_test/meshes/box.dae", scale, red_box_x, red_box_y, red_box_z, 0.0,0.0,0.707, 0.707);
    planning_scene.world.collision_objects.push_back(box_0);

    sorting_test.addCollisionMesh(box_1, group->getPlanningFrame(), "box1", "package://objects_sorting_test/meshes/box.dae", scale, blue_box_x, blue_box_y, blue_box_z, 0.0,0.0,0.707, 0.70);
    planning_scene.world.collision_objects.push_back(box_1);


    planning_scene.is_diff = true;
    planning_scene_diff_publisher.publish(planning_scene);

    //ros::Duration sleep_time(5.0);
    //sleep_time.sleep();

    ROS_INFO("Collision objects added.");

    // Subscribe /tag_detections
    // ^^^^^^^^^^^^^^^^^^^^^^^^^

    //go home

   sorting_test.goHome(group);

    //while(ros::ok()){


    //sub = node_handle.subscribe<apriltags_ros::AprilTagDetectionArray>("/tag_detections", 1, boost::bind(&SortingTest::detectionCallback, &sorting_test, _1, group));
     ros::Subscriber april_tag_sub = n.subscribe("/tag_detections", 10, getAprilTagPose);

        while (!april_tag_acquired_)
        {
            ros::spinOnce();
        }

        cout << "Apriltags acquired" << endl;
        april_tag_sub.shutdown();
        unsigned int size_at_array = april_tags_vector_.size();
        ROS_INFO("Apriltag vector size = %u", size_at_array);

        //Fix Tag offsets
        //^^^^^^^^^^^^^^^

        sorting_test.fixTagOffsets(april_tags_vector_, offset_x, offset_y, offset_z);

        planning_scene.world.collision_objects.clear();

        //Add collision objects
        //^^^^^^^^^^^^^^^^^^^^^

        std::vector<moveit_msgs::CollisionObject> cube;

        cube.resize(april_tags_vector_.size());

        for (unsigned int iter = 0; iter < april_tags_vector_.size(); iter++)
        {
            sorting_test.addCollisionBox(cube.at(iter), group->getPlanningFrame(), "cube_"+iter, 0.1, 0.1, 0.1,
                                            april_tags_vector_.at(iter).pose.pose.position.x ,
                                            april_tags_vector_.at(iter).pose.pose.position.y,
                                            april_tags_vector_.at(iter).pose.pose.position.z,
                                            april_tags_vector_.at(iter).pose.pose.orientation.x,
                                            april_tags_vector_.at(iter).pose.pose.orientation.y,
                                            april_tags_vector_.at(iter).pose.pose.orientation.z,
                                            april_tags_vector_.at(iter).pose.pose.orientation.w);

            planning_scene.world.collision_objects.push_back(cube.at(iter));

        }

        planning_scene_diff_publisher.publish(planning_scene);

        //Start the manipulation routine
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


        if(!april_tags_vector_.empty()){


          ofstream file;
          file.open(output_file.c_str());

             for(int j = 0; j < april_tags_vector_.size(); j++){

              bool cube_picked = sorting_test.pickCube(group, april_tags_vector_.at(j).pose.pose, pre_manipulation_offset, manipulation_offset,
                                    cube.at(j), group->getPlanningFrame(), "cube_"+j, planning_scene);

              if(cube_picked){
                string color;
                sorting_test.chooseRightBox(april_tags_vector_.at(j).id, color);

                if (color.compare("NULL") != 0){

                  geometry_msgs::Pose place_pose;

                  bool cube_placed;
                  if(sim == true){
                    cube_placed = sorting_test.placeCube(group, random, color,length, width, height, cube_edge, place_offset, cube.at(j),
                                                         group->getPlanningFrame(), "cube_"+j , planning_scene, place_pose);
                  }
                  else{
                    cube_placed = sorting_test.jointPlaceCube(group, color, cube.at(j), group->getPlanningFrame(), "cube_"+j ,
                                                              planning_scene, place_pose);
                  }

                  if(cube_placed){

                      file << "INPUT: [" << color << ", "
                                         << april_tags_vector_.at(j).pose.pose.position.x << ", "
                                         << april_tags_vector_.at(j).pose.pose.position.y << ", "
                                         << april_tags_vector_.at(j).pose.pose.position.z << ", "
                                         << april_tags_vector_.at(j).pose.pose.orientation.x << ", "
                                         << april_tags_vector_.at(j).pose.pose.orientation.y << ", "
                                         << april_tags_vector_.at(j).pose.pose.orientation.z << ", "
                                         << april_tags_vector_.at(j).pose.pose.orientation.w << "]\n";

                      file << "OUTPUT: [" << "pick()" << ", "
                                         << "place("
                                         << place_pose.position.x << ", "
                                         << place_pose.position.y << ", "
                                         << place_pose.position.z << ")]\n";


                  }

                }

              }

             // sorting_test.goHome(group);


          }

              file.close();

              ROS_INFO("END");


        }



    //}

    ros::waitForShutdown();
    return 0;
}
