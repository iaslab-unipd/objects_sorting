#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <apriltags_ros/AprilTagDetectionArray.h>
#include <apriltags_ros/AprilTagDetection.h>

#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

#include <robotiq_s_model_control/SModel_robot_output.h>


#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>

#include <geometric_shapes/shape_operations.h>

using namespace std;

namespace objects_sorting_test {

class SortingTest{

private:
    void defineGripper();
    double fRand(double fMin, double fMax);

public:

    ros::NodeHandle node_handle;
    ros::Publisher planning_scene_diff_publisher;
    ros::Publisher gripperPub;

    geometry_msgs::PoseStamped world_red_box_pose, world_blue_box_pose;

    robotiq_s_model_control::SModel_robot_output gripOpen, gripClose;

    //bool pose_established = false;

    apriltags_ros::AprilTagDetection manipulation_object;

    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

    SortingTest();
    virtual ~SortingTest() {};

    void addCollisionBox(moveit_msgs::CollisionObject& collision_object,
                            string frame_id, string id,
                            double dim_x, double dim_y, double dim_z,
                            double pos_x, double pos_y, double pos_z,
                            double or_x, double or_y, double or_z, double or_w);

    void addCollisionMesh(moveit_msgs::CollisionObject& collision_object,
                          string frame_id, string id,
                          string mesh, Eigen::Vector3d scale,
                           double pos_x, double pos_y, double pos_z,
                           double or_x, double or_y, double or_z, double or_w);

   //void detectionCallback(const apriltags_ros::AprilTagDetectionArray::ConstPtr& msg, moveit::planning_interface::MoveGroup* group);

    void fixTagOffsets(vector<apriltags_ros::AprilTagDetection>& april_tags_vector_,
                       double offset_x,
                       double offset_y,
                       double offset_z);

    double linearlyMove(moveit::planning_interface::MoveGroup* group, geometry_msgs::Pose goal);

    bool pickCube(moveit::planning_interface::MoveGroup* group, geometry_msgs::Pose cube_pose,
                  double pre_man_offset, double man_offset,
                  moveit_msgs::CollisionObject cube, string frame_id, string id, moveit_msgs::PlanningScene& planning_scene);

    void chooseRightBox(int apriltag_id, string& box_color);

    void defineRedBox(geometry_msgs::PoseStamped red_box_pose);
    void defineBlueBox(geometry_msgs::PoseStamped blue_box_pose);

    void randomPoseInBox(string& box_color, bool& random, geometry_msgs::Pose& random_pose,
                         double length, double width, double height, double cube_edge, double place_offset);

    void jointPoseInBox(string& box_color, std::vector<double>& joint_group_positions);

    bool jointPlaceCube(moveit::planning_interface::MoveGroup* group, string& box_color,
                  moveit_msgs::CollisionObject cube, string frame_id, string id, moveit_msgs::PlanningScene& planning_scene, geometry_msgs::Pose& place_pose);

    bool placeCube(moveit::planning_interface::MoveGroup* group, bool& random, string& box_color,
                  double length, double width, double height, double cube_edge, double place_offset,
                  moveit_msgs::CollisionObject cube, string frame_id, string id, moveit_msgs::PlanningScene& planning_scene, geometry_msgs::Pose& place_pose);


    bool goHome(moveit::planning_interface::MoveGroup* group);

    void openGripper();
    void closeGripper();
};
}
