#include "objects_sorting_test/test.h"

using namespace objects_sorting_test;

SortingTest::SortingTest(){

  planning_scene_diff_publisher = node_handle.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
  while(planning_scene_diff_publisher.getNumSubscribers() < 1)
  {
    ros::WallDuration sleep_t(0.5);
    sleep_t.sleep();
  }

  defineGripper();
  gripperPub = node_handle.advertise<robotiq_s_model_control::SModel_robot_output>("/robotiq_hands/l_hand/SModelRobotOutput",1);
}

void SortingTest::addCollisionBox(moveit_msgs::CollisionObject& collision_object,
                                     string frame_id, string id,
                                     double dim_x, double dim_y, double dim_z,
                                      double pos_x, double pos_y, double pos_z,
                                      double or_x, double or_y, double or_z, double or_w){

  shape_msgs::SolidPrimitive primitive;

  primitive.type = primitive.BOX;

  primitive.dimensions.resize(3);
  primitive.dimensions[0] = dim_x;
  primitive.dimensions[1] = dim_y;
  primitive.dimensions[2] = dim_z;

  geometry_msgs::Pose pose;
  pose.position.x = pos_x;
  pose.position.y = pos_y;
  pose.position.z = pos_z;
  pose.orientation.x = or_x;
  pose.orientation.y = or_y;
  pose.orientation.z = or_z;
  pose.orientation.w = or_w;

  collision_object.header.frame_id = frame_id;
  collision_object.id = id;
  collision_object.primitives.push_back(primitive);
  collision_object.primitive_poses.push_back(pose);
  collision_object.operation = collision_object.ADD;

}

void SortingTest::addCollisionMesh(moveit_msgs::CollisionObject& collision_object,
                                   string frame_id, string id,
                                   string mesh_file, Eigen::Vector3d scale,
                                    double pos_x, double pos_y, double pos_z,
                                    double or_x, double or_y, double or_z, double or_w){


     shapes::Mesh* m = shapes::createMeshFromResource(mesh_file, scale);

     shape_msgs::Mesh mesh;
     shapes::ShapeMsg mesh_msg;
     shapes::constructMsgFromShape(m, mesh_msg);
     mesh = boost::get<shape_msgs::Mesh>(mesh_msg);

     collision_object.meshes.resize(1);
     collision_object.mesh_poses.resize(1);
     collision_object.meshes[0] = mesh;

     collision_object.meshes.push_back(mesh);

     collision_object.header.frame_id = frame_id;
     collision_object.id = id;

     geometry_msgs::Pose pose;
     pose.position.x = pos_x;
     pose.position.y = pos_y;
     pose.position.z = pos_z;
     pose.orientation.x = or_x;
     pose.orientation.y = or_y;
     pose.orientation.z = or_z;
     pose.orientation.w = or_w;
     collision_object.mesh_poses.push_back(pose);

     collision_object.operation = collision_object.ADD;

}
/*
void SortingTest::detectionCallback(const apriltags_ros::AprilTagDetectionArray::ConstPtr& msg, moveit::planning_interface::MoveGroup* group)
{
    double correction_offset_x = 0.03;
    double correction_offset_y = 0.015;
    double z_offset = 0.05;

   tf::TransformListener listener;
   geometry_msgs::PoseStamped world_cube_pose, camera_cube_pose;
   moveit_msgs::CollisionObject cube;
   moveit_msgs::PlanningScene planning_scene;

   if (msg->detections.size() > 0 && !pose_established){
    //Add every detection as a collision object
    for(int i = 0; i < msg->detections.size(); i++){

      try
      {
        ROS_INFO("Detected cube - Transforming pose!");

        listener.waitForTransform("world", "camera_ir_optical_frame", ros::Time::now(), ros::Duration(1.0));
        camera_cube_pose = msg->detections.at(i).pose;
        listener.transformPose("world", camera_cube_pose, world_cube_pose);
        ROS_INFO("Detected cube - Pose transformation done!");

        ROS_INFO("Adding collision objects into the world.");

        //adding offset
        world_cube_pose.pose.position.x += correction_offset_x;
        world_cube_pose.pose.position.y += correction_offset_y;
        world_cube_pose.pose.position.z -= z_offset;

        addCollisionObject(cube, group->getPlanningFrame(), "cube_"+i, 0.1, 0.1, 0.1,
                                        world_cube_pose.pose.position.x , world_cube_pose.pose.position.y, world_cube_pose.pose.position.z,
                                        world_cube_pose.pose.orientation.x, world_cube_pose.pose.orientation.y, world_cube_pose.pose.orientation.z, world_cube_pose.pose.orientation.w);

        planning_scene.world.collision_objects.push_back(cube);

        planning_scene.is_diff = true;
        planning_scene_diff_publisher.publish(planning_scene);

      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("Nope! %s", ex.what());
        pose_established = false;
      }

    }

    }

}
*/

void SortingTest::fixTagOffsets(vector<apriltags_ros::AprilTagDetection>& april_tags_vector_,
                                double offset_x,
                                double offset_y,
                                double offset_z){

  if(!april_tags_vector_.empty()){

    for(unsigned int i = 0; i < april_tags_vector_.size(); i++){
      april_tags_vector_.at(i).pose.pose.position.x += offset_x;
      april_tags_vector_.at(i).pose.pose.position.y += offset_y;
      april_tags_vector_.at(i).pose.pose.position.z += offset_z;
    }
  }
}
void SortingTest::openGripper(){
  gripperPub.publish(gripOpen);
  ros::spinOnce();
}

void SortingTest::closeGripper(){
  gripperPub.publish(gripClose);
  ros::spinOnce();
}


double SortingTest::linearlyMove(moveit::planning_interface::MoveGroup* group, geometry_msgs::Pose goal){

  group->setStartState(*group->getCurrentState());

  std::vector<geometry_msgs::Pose> waypoints;

  waypoints.push_back(goal);

  const double jump_threshold = 0.0;
  const double eef_step = 0.02;
  double fraction = 0;

  moveit_msgs::RobotTrajectory trajectory_msg;
  group->setPlanningTime(10.0);

  fraction = group->computeCartesianPath(waypoints,
                                               eef_step,  // eef_step
                                               jump_threshold,   // jump_threshold
                                               trajectory_msg, false);
  // The trajectory needs to be modified so it will include velocities as well.
  // First to create a RobotTrajectory object
  robot_trajectory::RobotTrajectory rt(group->getCurrentState()->getRobotModel(), "manipulator");

  // Second get a RobotTrajectory from trajectory
  rt.setRobotTrajectoryMsg(*group->getCurrentState(), trajectory_msg);

  // Thrid create a IterativeParabolicTimeParameterization object
  trajectory_processing::IterativeParabolicTimeParameterization iptp;

  // Fourth compute computeTimeStamps
  bool time_success = iptp.computeTimeStamps(rt);
  ROS_INFO("Computed time stamp %s",time_success?"SUCCEDED":"FAILED");

  // Get RobotTrajectory_msg from RobotTrajectory
  rt.getRobotTrajectoryMsg(trajectory_msg);

  // Finally plan and execute the trajectory
  moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  my_plan.trajectory_ = trajectory_msg;
  ROS_INFO("Cartesian path: %.2f%% acheived",fraction * 100.0);

  bool execution_success = (group->execute(my_plan)  == moveit::planning_interface::MoveItErrorCode::SUCCESS);

  if (!execution_success){
    ROS_ERROR("Execution failed while linearly moving.");
    fraction = 0;
  }

  return fraction;
}

bool SortingTest::pickCube(moveit::planning_interface::MoveGroup* group, geometry_msgs::Pose cube_pose, double pre_man_offset, double man_offset,
                           moveit_msgs::CollisionObject cube, string frame_id, string id, moveit_msgs::PlanningScene& planning_scene){


  group->setStartStateToCurrentState();

    tf::Quaternion gripper_orientation;
    gripper_orientation.setW(0.707);
    gripper_orientation.setY(0.707);


    tf::Quaternion cube_orientation;
    tf::quaternionMsgToTF(cube_pose.orientation, cube_orientation);

    const tf::Quaternion gripper_final_orientation = cube_orientation * gripper_orientation;

  geometry_msgs::Pose manipulation_pose, pre_manipulation_pose;


  manipulation_pose.position = cube_pose.position;
  tf::quaternionTFToMsg(gripper_final_orientation, manipulation_pose.orientation);

  pre_manipulation_pose.position  = cube_pose.position;
  tf::quaternionTFToMsg(gripper_final_orientation, pre_manipulation_pose.orientation);

  manipulation_pose.position.z += man_offset;
  pre_manipulation_pose.position.z += pre_man_offset;

  group->setPoseTarget(pre_manipulation_pose);

  group->setGoalTolerance(0.05);

  moveit::planning_interface::MoveGroupInterface::Plan my_plan;

  //Plan to the pre-manipulation pose

  bool success = false;
  double fraction = 0;
  int trials = 0;

  while(!success && trials < 100){

      success = (group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
      trials++;
  }

  if(success){

    success = false;
    trials = 0;
    while(!success && trials < 100){

        success = (group->move()  == moveit::planning_interface::MoveItErrorCode::SUCCESS);
        trials++;

        cout << trials << endl;

        cout << success << endl;
    }

      if(success){
        trials = 0;

        // Plan to the manipulation pose

        while(fraction < 0.5 && trials < 100){
          fraction = linearlyMove(group, manipulation_pose);
          trials++;
        }
        if(fraction > 0.5){

          //attach object to the robot

          /* First, define the REMOVE object message*/

          moveit_msgs::AttachedCollisionObject attached_object;
          attached_object.object = cube;
          attached_object.link_name = "wrist_3_link";

          moveit_msgs::CollisionObject remove_object;
          remove_object.id = id;
          remove_object.header.frame_id = frame_id;
          remove_object.operation = remove_object.REMOVE;

          /* Carry out the REMOVE + ATTACH operation */
          ROS_INFO("Attaching the object to the right wrist and removing it from the world.");
          planning_scene.world.collision_objects.clear();
          planning_scene.world.collision_objects.push_back(remove_object);
          planning_scene.robot_state.attached_collision_objects.push_back(attached_object);
          planning_scene_diff_publisher.publish(planning_scene);
          ros::spinOnce();

          //sleep(1.0);

          //close the gripper

          closeGripper();
          sleep(3.0);
          ROS_INFO("Gripper closed");

          //back to pre manipulation pose

          fraction = 0;
          trials = 0;

          while(fraction < 0.5 && trials < 100){
            fraction = linearlyMove(group, pre_manipulation_pose);
            trials++;
          }
          if(fraction > 0.5){
            ROS_INFO("Object picked");
            success = true;

          }
          else{
            ROS_ERROR("Failed to come back.");
            success = false;
            goHome(group);
          }

        }
        else{
          ROS_ERROR("Manipulation pose has not been reached.");
          success = false;
          goHome(group);
        }
      }
      else{
        ROS_ERROR("Pre manipulation pose has not been reached.");
        goHome(group);
      }


  }
  else{
    ROS_ERROR("Pre manipulation pose has not been reached.");
    goHome(group);
  }

  return success;
}

void SortingTest::chooseRightBox(int apriltag_id, string& box_color){

  if(apriltag_id == 0 || apriltag_id == 1 || apriltag_id == 2 || apriltag_id == 3){
    box_color = "red";
    ROS_INFO("Red cube in red box");
  }
  else if(apriltag_id == 4 || apriltag_id == 5 || apriltag_id == 6 || apriltag_id == 7){
    box_color = "blue";
    ROS_INFO("Blue cube in blue box");
  }
  else{
    box_color = "NULL";
  }
}

void SortingTest::defineRedBox(geometry_msgs::PoseStamped red_box_pose){

  try
  {
    ROS_INFO("Transforming red box pose in \world ref_system!");
    tf::TransformListener listener;
    listener.waitForTransform("world", red_box_pose.header.frame_id, ros::Time::now(), ros::Duration(1.0));
    listener.transformPose("world", red_box_pose, world_red_box_pose);
    ROS_INFO("Red box pose transformation done!");

    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("Nope! %s", ex.what());
    }

}

void SortingTest::defineBlueBox(geometry_msgs::PoseStamped blue_box_pose){

  try
  {
    ROS_INFO("Transforming blue box pose in \world ref_system!");
    tf::TransformListener listener;
    listener.waitForTransform("world", blue_box_pose.header.frame_id, ros::Time::now(), ros::Duration(1.0));
    listener.transformPose("world", blue_box_pose, world_blue_box_pose);
    ROS_INFO("Blue box pose transformation done!");

    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("Nope! %s", ex.what());
    }

}

void SortingTest::randomPoseInBox(string& box_color, bool& random, geometry_msgs::Pose& random_pose,
                                  double length, double width, double height, double cube_edge, double place_offset){

    geometry_msgs::Pose box_pose;

    cout << "Random pose in box: "<< box_color << endl;

    cout << "red: " << box_color.compare("red") << endl;

    cout << "blue: " << box_color.compare("blue") << endl;

    if(box_color.compare("red") == 0){
      box_pose = world_red_box_pose.pose;

      cout << "red" << endl;
    }
    else if(box_color.compare("blue") == 0) {
      box_pose = world_blue_box_pose.pose;

      cout << "blue" << endl;
    }

    if(box_color.compare("red") == 0 || box_color.compare("blue") == 0){

      if(random == true){
        random_pose.position.x = fRand(box_pose.position.x - width/2 + cube_edge, box_pose.position.x + width/2 - cube_edge);
        random_pose.position.y = fRand(box_pose.position.y - length/2 + cube_edge, box_pose.position.y + length/2 - cube_edge);

      }
      else{
        random_pose.position.x = box_pose.position.x; //fRand(box_pose.position.x - width/2 + cube_edge, box_pose.position.x + width/2 - cube_edge);
        random_pose.position.y = box_pose.position.y; //fRand(box_pose.position.y - length/2 + cube_edge, box_pose.position.y + length/2 - cube_edge);

      }
      random_pose.position.z = box_pose.position.z + place_offset;
      random_pose.orientation.w = 0.707;
      random_pose.orientation.y = 0.707;


      cout << "randomPoseInBox result: "<< endl;
      cout << random_pose.position.x << endl;
      cout << random_pose.position.y << endl;
      cout << random_pose.position.z << endl;

    }
    else{
      ROS_ERROR("No box correcly identified");
    }
}

void SortingTest::jointPoseInBox(string& box_color, std::vector<double>& joint_group_positions){

  if(box_color.compare("red") == 0){

    joint_group_positions[0] = -2.3;  // radians
    joint_group_positions[1] = -1.39626;
    joint_group_positions[2] = 1.369626;
    joint_group_positions[3] = -1.5708;
    joint_group_positions[4] = -1.5708;
    joint_group_positions[5] = 0;

    cout << "red" << endl;
  }
  else if(box_color.compare("blue") == 0) {


    joint_group_positions[0] = -2.0944;  // radians
    joint_group_positions[1] = -0.872665;
    joint_group_positions[2] = 0.733038;
    joint_group_positions[3] = -1.5708;
    joint_group_positions[4] = -1.5708;
    joint_group_positions[5] = 0;

    cout << "blue" << endl;
  }
  else{
    ROS_ERROR("No box correcly identified");
  }
}

bool SortingTest::jointPlaceCube(moveit::planning_interface::MoveGroup* group, string& box_color,
                                  moveit_msgs::CollisionObject cube, string frame_id, string id, moveit_msgs::PlanningScene& planning_scene,
                                 geometry_msgs::Pose& place_pose){

  std::vector<double> joint_group_positions;

  moveit::core::RobotStatePtr current_state = group->getCurrentState();

  const robot_state::JointModelGroup* joint_model_group = group->getCurrentState()->getJointModelGroup("manipulator");

  current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);

  jointPoseInBox(box_color, joint_group_positions);

  robot_state::RobotState start_state(*group->getCurrentState());
  group->setStartState(start_state);

  group->setJointValueTarget(joint_group_positions);

  cout << "joint_group_positions[0]: " << joint_group_positions[0]<< endl;
  moveit::planning_interface::MoveGroupInterface::Plan my_plan;

  //Plan to the pre-manipulation pose

  bool success = false;
  double fraction = 0;
  int trials = 0;

  while(!success && trials < 100){

      success = (group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
      trials++;
  }

  if(success){
      success = (group->move() == moveit::planning_interface::MoveItErrorCode::SUCCESS);

      if(success){

        geometry_msgs::PoseStamped current_pose = group->getCurrentPose(group->getEndEffectorLink());
        place_pose = current_pose.pose;
        openGripper();
        ROS_INFO("Gripper opened");
        sleep(1.0);


        /* First, define the DETACH object message*/

        moveit_msgs::AttachedCollisionObject detach_object;
        detach_object.object = cube;
        detach_object.link_name = "wrist_3_link";

        detach_object.object.operation = cube.REMOVE;

        /* Carry out the DETACH + ADD operation */
        ROS_INFO("Detaching the object from the robot.");// and returning it to the world.");
        planning_scene.robot_state.attached_collision_objects.clear();
        planning_scene.robot_state.attached_collision_objects.push_back(detach_object);
        //planning_scene.world.collision_objects.clear();
        planning_scene.world.collision_objects.push_back(detach_object.object);
        planning_scene_diff_publisher.publish(planning_scene);
        ros::spinOnce();
        sleep(1.0);
      }
      else{
        ROS_ERROR("Place pose has not been reached.");
        goHome(group);
      }

  }
  else{
    ROS_ERROR("Place pose has not been reached.");
    goHome(group);
  }

  return success;

}

bool SortingTest::placeCube(moveit::planning_interface::MoveGroup* group, bool& random, string& box_color,
          double length, double width, double height, double cube_edge, double place_offset,
          moveit_msgs::CollisionObject cube, string frame_id, string id, moveit_msgs::PlanningScene& planning_scene, geometry_msgs::Pose& place_pose){

  geometry_msgs::Pose random_pose;

  randomPoseInBox(box_color, random, random_pose, length, width, height, cube_edge, place_offset);

  cout << "randomPoseInBox result from placeCube: "<< endl;
  cout << random_pose.position.x << endl;
  cout << random_pose.position.y << endl;
  cout << random_pose.position.z << endl;


  place_pose = random_pose;

  robot_state::RobotState start_state(*group->getCurrentState());
  group->setStartState(start_state);

  group->setPoseTarget(random_pose);

  moveit::planning_interface::MoveGroupInterface::Plan my_plan;

  //Plan to the pre-manipulation pose

  bool success = false;
  double fraction = 0;
  int trials = 0;

  while(!success && trials < 100){

      success = (group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
      trials++;
  }

  if(success){
      success = (group->move()  == moveit::planning_interface::MoveItErrorCode::SUCCESS);

      if(success){
        openGripper();
        ROS_INFO("Gripper opened");
        sleep(1.0);


        /* First, define the DETACH object message*/

        moveit_msgs::AttachedCollisionObject detach_object;
        detach_object.object = cube;
        detach_object.link_name = "wrist_3_link";

        detach_object.object.operation = cube.REMOVE;

        /* Carry out the DETACH + ADD operation */
        ROS_INFO("Detaching the object from the robot.");// and returning it to the world.");
        planning_scene.robot_state.attached_collision_objects.clear();
        planning_scene.robot_state.attached_collision_objects.push_back(detach_object);
        //planning_scene.world.collision_objects.clear();
        planning_scene.world.collision_objects.push_back(detach_object.object);
        planning_scene_diff_publisher.publish(planning_scene);
        ros::spinOnce();
        sleep(1.0);
      }
      else{
        ROS_ERROR("Place pose has not been reached.");
        goHome(group);
      }

  }
  else{
    ROS_ERROR("Place pose has not been reached.");
    goHome(group);
  }

  return success;


}

bool SortingTest::goHome(moveit::planning_interface::MoveGroup* group){

 //name: [elbow_joint, shoulder_lift_joint, shoulder_pan_joint, wrist_1_joint, wrist_2_joint, wrist_3_joint]
 //position: [1.5708, -2.26893, -1.5708, -1.0472, -1.5708, 0]

  moveit::core::RobotStatePtr current_state = group->getCurrentState();

  const robot_state::JointModelGroup* joint_model_group = group->getCurrentState()->getJointModelGroup("manipulator");

  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);

  joint_group_positions[0] = -1.5708;  // radians
  joint_group_positions[1] = -1.91986;
  joint_group_positions[2] = 1.5708;
  joint_group_positions[3] = -1.5708;
  joint_group_positions[4] = -1.5708;
  joint_group_positions[5] = 0;

  group->setJointValueTarget(joint_group_positions);

  /*geometry_msgs::Pose home;

  home.position.x = 0.30;
  home.position.y = 0;
  home.position.z = 1.6;
  home.orientation.w = 0.707;
  home.orientation.y = 0.707;*/

  group->setStartStateToCurrentState();


 // group->setPoseTarget(home);

  moveit::planning_interface::MoveGroupInterface::Plan my_plan;

  //Plan to the pre-manipulation pose

  bool success = false;
  int trials = 0;

  while(!success && trials < 100){

      success = (group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
      trials++;
  }

  if(success){
    success = (group->move()  == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    if(success){
      openGripper();
      ROS_INFO("Back to home position");
      sleep(1.0);
      ros::spinOnce();
    }
    else
      ROS_ERROR("Failed to go back at Home position");

  }
  else
    ROS_ERROR("Failed to go back at Home position");

  return success;
}

double SortingTest::fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void SortingTest::defineGripper()
{
  gripOpen.rACT = 1;
  gripOpen.rMOD = 0;
  gripOpen.rGTO = 1;
  gripOpen.rATR = 0;
  gripOpen.rGLV = 0;
  gripOpen.rICF = 0;
  gripOpen.rICS = 0;
  gripOpen.rPRA = 1;
  gripOpen.rSPA = 255;
  gripOpen.rFRA = 150;
  gripOpen.rPRB = 0;
  gripOpen.rSPB = 0;
  gripOpen.rFRB = 0;
  gripOpen.rPRC = 0;
  gripOpen.rSPC = 0;
  gripOpen.rFRC = 0;
  gripOpen.rPRS = 0;
  gripOpen.rSPS = 0;
  gripOpen.rFRS = 0;

  //grip closed params
  gripClose.rACT = 1;
  gripClose.rMOD = 0;
  gripClose.rGTO = 1;
  gripClose.rATR = 0;
  gripClose.rGLV = 0;
  gripClose.rICF = 0;
  gripClose.rICS = 0;
  gripClose.rPRA = 255;
  gripClose.rSPA = 255;
  gripClose.rFRA = 150;
  gripClose.rPRB = 0;
  gripClose.rSPB = 0;
  gripClose.rFRB = 0;
  gripClose.rPRC = 0;
  gripClose.rSPC = 0;
  gripClose.rFRC = 0;
  gripClose.rPRS = 0;
  gripClose.rSPS = 0;
  gripClose.rFRS = 0;
}


